package com.example.oauthserver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.core.ClientAuthenticationMethod;
import org.springframework.security.oauth2.core.oidc.OidcScopes;
import org.springframework.security.oauth2.jose.jws.SignatureAlgorithm;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClient;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClientRepository;
import org.springframework.security.oauth2.server.authorization.settings.ClientSettings;
import org.springframework.security.oauth2.server.authorization.settings.OAuth2TokenFormat;
import org.springframework.security.oauth2.server.authorization.settings.TokenSettings;

import java.time.Duration;
import java.util.UUID;

@SpringBootApplication
public class OauthserverApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(OauthserverApplication.class, args);
    }

    @Autowired
    RegisteredClientRepository repo;

    @Override
    public void run(String... args) throws Exception {

        RegisteredClient registeredClient = RegisteredClient.withId("articles-client")
                .clientId("articles-client")
                .clientSecret("secret")
                .clientName("Articles Client")
                .clientAuthenticationMethod(ClientAuthenticationMethod.CLIENT_SECRET_BASIC)
                .authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)
                .redirectUri("http://127.0.0.1:8080/login/oauth2/code/articles-client-oidc")
                .redirectUri("http://127.0.0.1:8080/authorized")
                .scope(OidcScopes.OPENID)
                .scope(OidcScopes.PROFILE)
                .scope("articles.read")
                .postLogoutRedirectUri("http://127.0.0.1:8080/articles")
                .clientSettings(
                        ClientSettings.builder()
                                .requireAuthorizationConsent(false)
//                                .requireProofKey(false)
                                .build())
                .build();

        repo.save(registeredClient);
    }
}
