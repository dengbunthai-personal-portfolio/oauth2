package com.example.oauthserverpkce;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.core.ClientAuthenticationMethod;
import org.springframework.security.oauth2.core.oidc.OidcScopes;
import org.springframework.security.oauth2.jose.jws.SignatureAlgorithm;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClient;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClientRepository;
import org.springframework.security.oauth2.server.authorization.settings.ClientSettings;
import org.springframework.security.oauth2.server.authorization.settings.OAuth2TokenFormat;
import org.springframework.security.oauth2.server.authorization.settings.TokenSettings;

import java.time.Duration;
import java.util.UUID;

@SpringBootApplication
public class OauthserverApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(OauthserverApplication.class, args);
    }

    @Autowired
    RegisteredClientRepository repo;

    @Override
    public void run(String... args) throws Exception {

//        RegisteredClient registeredClient = RegisteredClient.withId("articles-client")
//                .clientId("articles-client")
//                .clientSecret("{noop}secret")
//                .clientName("Articles Client")
//                .clientAuthenticationMethod(ClientAuthenticationMethod.CLIENT_SECRET_BASIC)
//                .authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)
//                .redirectUri("http://127.0.0.1:8080/login/oauth2/code/articles-client-oidc")
//                .redirectUri("http://127.0.0.1:8080/authorized")
//                .scope(OidcScopes.OPENID)
//                .scope(OidcScopes.PROFILE)
//                .scope("articles.read")
//                .postLogoutRedirectUri("http://127.0.0.1:8080/articles")
//                .clientSettings(
//                        ClientSettings.builder()
//                                .requireAuthorizationConsent(true)
//                                .requireProofKey(true)
//                                .build())
//                .build();

        RegisteredClient registeredClient = RegisteredClient.withId("articles-client")
                .clientId("articles-client")
                .clientAuthenticationMethods(s -> {
                    s.add(ClientAuthenticationMethod.NONE);
                })
                .authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)
                .redirectUri("http://127.0.0.1:8080/login/oauth2/code/articles-client-oidc")
                .redirectUri("")
                .postLogoutRedirectUri("http://127.0.0.1:8080/articles")
                .scope("message.read")
                .clientSettings(ClientSettings.builder()
                        .requireAuthorizationConsent(true)
                        .requireProofKey(true) // Only PKCE is supported
                        .build())
                .tokenSettings(TokenSettings.builder()
                        .accessTokenFormat(OAuth2TokenFormat.SELF_CONTAINED)
                        .idTokenSignatureAlgorithm(SignatureAlgorithm.RS256)
                        .accessTokenTimeToLive(Duration.ofSeconds(30 * 60))
                        .refreshTokenTimeToLive(Duration.ofSeconds(60 * 60))
                        .reuseRefreshTokens(true)
                        .build())
                .build();

        RegisteredClient otherClient = RegisteredClient.withId("other-client")
                .clientId("other-client")
                .clientSecret("{noop}secret")
                .clientAuthenticationMethods(s -> {
                    s.add(ClientAuthenticationMethod.CLIENT_SECRET_BASIC);
                })
                .authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)
                .redirectUri("http://127.0.0.1:8081/login/oauth2/code/other-client-oidc")
                .postLogoutRedirectUri("http://127.0.0.1:8081/other")
                .scope("message.read")
                .clientSettings(ClientSettings.builder()
                        .requireAuthorizationConsent(false)
                        .requireProofKey(false) // Only PKCE is supported
                        .build())
                .tokenSettings(TokenSettings.builder()
                        .accessTokenFormat(OAuth2TokenFormat.SELF_CONTAINED)
                        .idTokenSignatureAlgorithm(SignatureAlgorithm.RS256)
                        .accessTokenTimeToLive(Duration.ofSeconds(30 * 60))
                        .refreshTokenTimeToLive(Duration.ofSeconds(60 * 60))
                        .reuseRefreshTokens(true)
                        .build())
                .build();

        repo.save(registeredClient);
        repo.save(otherClient);

    }


	/*


      authorizationserver:
        issuer: http://localhost:9000
        client:
          articles-client:
            registration:
              client-id: articles-client
              client-secret: "{noop}secret"
              client-name: Articles Client
              client-authentication-methods:
                - client_secret_basic
              authorization-grant-types:
                - authorization_code
                - refresh_token
              redirect-uris:
                - http://127.0.0.1:8080/login/oauth2/code/articles-client-oidc
                - http://127.0.0.1:8080/authorized
              scopes:
                - openid
                - articles.read
            require-authorization-consent: true

	* */
}
