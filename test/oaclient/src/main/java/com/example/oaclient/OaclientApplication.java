package com.example.oaclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OaclientApplication {

	public static void main(String[] args) {
		SpringApplication.run(OaclientApplication.class, args);
	}

}
